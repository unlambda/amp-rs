This is a Rust implementation of the [AMP protocol].

AMP is a protocol for asynchronous RPC originally implemented in [Twisted], a
framework for asynchronous networking in Python.  See the [Twisted AMP HOWTO]
and [Twisted API docs] for details on how it is intended to be used.  The
protocol is described in detail at https://amp-protocol.net.

This crate supports serialization and deserialization of AMP via [`serde`].

## TODO

1. Allow commands that don't expect a response, to allow for more graceful
   "quit" commands
2. Async endpoint
3. Reconsider module structure
4. Reconsider error organization
5. Support more AMP data types

# License

Copyright 2016 EditShare LLC.

`amp-rs` is licensed under the terms of either the Apache License, Version
2.0, or the MIT (Expat) license, at your choice.  See LICENSE-APACHE or
LICENSE-MIT included in the repository for the full licenses.  You may also
obtain copies of the licenses at

    http://www.apache.org/licenses/LICENSE-2.0
    https://opensource.org/licenses/MIT
    http://directory.fsf.org/wiki/License:Expat

Unless required by applicable law or agreed to in writing, software
distributed under either license is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

[AMP Protocol]: https://amp-protocol.net
[Twisted AMP HOWTO]: http://twistedmatrix.com/documents/current/core/howto/amp.html
[Twisted]: http://twistedmatrix.com/
[Twisted API docs]:
http://twistedmatrix.com/documents/current/api/twisted.protocols.amp.html
[`serde`]: https://github.com/serde-rs/serde
