#![feature(custom_derive, plugin)]
#![plugin(serde_macros)]

extern crate serde;
#[macro_use]
extern crate amp;

use std::cell::Cell;
use std::io::BufReader;
use std::io::prelude::*;
use std::net::{TcpStream, TcpListener};
use std::process::{Command, Stdio};

use amp::protocol::SyncEndpoint;

struct TestProcess(std::process::Child);

impl TestProcess {
    fn new(test_name: &str) -> Self {
        TestProcess(Command::new("python")
            .arg("tests/twisty.py")
            .arg(test_name)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::inherit())
            .spawn()
            .unwrap())
    }

    fn writer(&mut self) -> &mut std::process::ChildStdin {
        self.0.stdin.as_mut().unwrap()
    }

    fn reader(&mut self) -> &mut std::process::ChildStdout {
        self.0.stdout.as_mut().unwrap()
    }
}

impl Drop for TestProcess {
    fn drop(&mut self) {
        self.0.stdin = None;
        let status = self.0.wait().unwrap();
        assert!(status.success());
    }
}

#[test]
fn serialize_struct() {
    #[derive(Serialize, Deserialize)]
    struct Point {
        x: i32,
        y: i32,
    }

    let mut process = TestProcess::new("serialize_struct");
    amp::serde::to_writer(process.writer(), &Point { x: 5, y: 10 }).unwrap();
}

#[test]
fn serialize_list() {
    let mut process = TestProcess::new("serialize_list");
    amp::serde::to_writer(process.writer(), &[1.1, 2.2, 3.3, 4.4, 5.5])
        .unwrap();
}

#[test]
fn serialize_nested() {
    #[derive(Serialize)]
    struct Inner {
        foo: i32,
        bar: i64,
    }

    #[derive(Serialize)]
    struct Outer {
        inner: Vec<Inner>,
    }

    #[derive(Serialize)]
    struct Nest {
        lists: Vec<Vec<&'static str>>,
        structs: Vec<Inner>,
        outer: Vec<Outer>,
    }

    let nest = Nest {
        lists: vec![vec!["foo", "bar", "baz"],
                    vec!["lorem", "ipsum", "dolor"],
                    vec!["prisencolinensinainciusol", "alright!"]],
        structs: vec![Inner { foo: 2, bar: 123 }],
        outer: vec![Outer { inner: vec![] },
                    Outer {
                        inner: vec![Inner {
                                        foo: 867,
                                        bar: 5309,
                                    },
                                    Inner {
                                        foo: 555,
                                        bar: 1212,
                                    }],
                    }],
    };
    let nest2 = Nest {
        lists: vec![],
        structs: vec![],
        outer: vec![],
    };

    let mut process = TestProcess::new("serialize_nested");
    amp::serde::to_writer(process.writer(), &nest).unwrap();
    amp::serde::to_writer(process.writer(), &nest2).unwrap();
}

#[test]
fn deserialize_struct() {
    #[derive(Deserialize, PartialEq, Eq, Debug)]
    struct Test {
        s: String,
        b: serde::bytes::ByteBuf,
    }

    let mut process = TestProcess::new("deserialize_struct");
    let s: Test = amp::serde::from_reader(process.reader()).unwrap();

    assert_eq!(s,
               Test {
                   s: "λλλ".into(),
                   b: b"123\x00\xFF".to_vec().into(),
               })
}

#[test]
fn deserialize_nested() {
    #[derive(Deserialize, PartialEq, Eq, Debug)]
    struct Four {
        v: Vec<std::collections::HashMap<String, String>>,
    }

    #[derive(Deserialize, PartialEq, Eq, Debug)]
    struct Three {
        s: Four,
    }

    #[derive(Deserialize, PartialEq, Eq, Debug)]
    struct Two {
        s: Three,
    }

    #[derive(Deserialize, PartialEq, Eq, Debug)]
    struct Nestier {
        s: Two,
    }

    let mut process = TestProcess::new("deserialize_nested");
    let s: Nestier = amp::serde::from_reader(process.reader()).unwrap();

    let mut hm = std::collections::HashMap::new();
    hm.insert("foo".into(), "Bar".into());
    hm.insert("baz".into(), "Quux".into());
    let hm2 = hm.clone();
    assert_eq!(s,
               Nestier {
                   s: Two { s: Three { s: Four { v: vec![hm, hm2] } } },
               });
}

command! {
    struct Echo {
        arguments: EchoArgs { message: String },
        response: EchoResponse { message: String },
        errors: EchoErrors { ..UNKNOWN }
    }
}

command! {
    struct Ohce {
        arguments: OhceArgs { message: String },
        response: OhceResponse { egassem: String },
        errors: OhceErrors { ..UNKNOWN }
    }
}

command! {
    struct Quit {
        arguments: QuitArgs {},
        response: QuitResponse {},
        errors: QuitErrors { ..UNKNOWN }
    }
}

#[test]
fn amp_client() {
    let mut process = TestProcess::new("amp_server");

    process.0.stdin = None;
    let mut stdout = process.0.stdout.take().unwrap();
    let mut reader = BufReader::new(&mut stdout);
    let mut buf = String::new();
    let _ = reader.read_line(&mut buf).unwrap();
    assert_eq!(buf, "running\n");

    let stream = TcpStream::connect("localhost:4242").unwrap();

    let mut endpoint = SyncEndpoint::new(stream);

    assert_eq!("sesquipedalian", endpoint.call(
        Echo,
        EchoArgs { message: "sesquipedalian".into() }
    ).unwrap().message);
    assert_eq!(
        "brobdingnagian".chars().rev().collect::<String>(),
        endpoint.call(
            Ohce,
            OhceArgs { message: "brobdingnagian".into() }
        ).unwrap().egassem
    );


    endpoint.call(Quit, QuitArgs {}).unwrap();
}

#[test]
fn amp_server() {
    let listener = TcpListener::bind("127.0.0.1:2424").unwrap();

    let mut process = TestProcess::new("amp_client");
    process.0.stdin = None;
    process.0.stdout = None;

    let (socket, _addr) = listener.accept().unwrap();

    let quit_received = Cell::new(false);

    let mut endpoint = SyncEndpoint::new(socket);

    endpoint.register(
        Echo,
        |args: EchoArgs| Ok(EchoResponse { message: args.message })
    );
    endpoint.register(
        Ohce,
        |args: OhceArgs| Ok(OhceResponse {
            egassem: args.message.chars().rev().collect::<String>()
        })
    );
    endpoint.register(
        Quit,
        |_: QuitArgs| { quit_received.set(true); Ok(QuitResponse {}) }
    );

    let mut count = 0;
    while !quit_received.get() {
        count += 1;
        if count > 3 {
            panic!("Too many requests without receiving quit!");
        }
        match endpoint.respond_once() {
            Err(e) => panic!("Responding to command failed: {:?}", e),
            _ => ()
        }
    }
}
