# -*- coding: utf-8 -*-

import argparse
import sys
import pprint

from twisted.internet import reactor
from twisted.internet.defer import (
    DeferredList,
    inlineCallbacks,
)
from twisted.internet.protocol import (
    ClientCreator,
    Factory,
)
from twisted.protocols import amp


def assert_deserializes(arg, serialized, value):
    box = amp.AmpBox()
    arg.fromBox('serialized',
                {'serialized': serialized},
                box,
                amp.AMP)
    assert box['serialized'] == value, \
        ("serialized: %s\nexpected:\n%s\ngot:\n%s" %
         (repr(serialized),
          pprint.pformat(value),
          pprint.pformat(box['serialized'])))


def serialize_to_stdout(arg, value):
    box = amp.AmpBox()
    arg.toBox("serialized",
              box,
              {"serialized": value},
              amp.AMP)
    sys.stdout.write(box["serialized"])


def serialize_struct():
    serialized = sys.stdin.read()
    arg = amp.AmpList([('x', amp.Integer()),
                       ('y', amp.Integer())])
    assert_deserializes(arg, serialized, [{'x': 5, 'y': 10}])


def serialize_list():
    serialized = sys.stdin.read()
    arg = amp.ListOf(amp.Float())
    assert_deserializes(arg, serialized, [1.1, 2.2, 3.3, 4.4, 5.5])


def serialize_nested():
    serialized = sys.stdin.read()
    inner_list = amp.AmpList([("foo", amp.Integer()),
                              ("bar", amp.Integer())])
    outer_list = amp.AmpList([("inner", inner_list)])
    arg = amp.AmpList([("lists", amp.ListOf(amp.ListOf(amp.String()))),
                       ("structs", inner_list),
                       ("outer", outer_list)])
    assert_deserializes(arg, serialized, [
        {"lists": [["foo", "bar", "baz"],
                   ["lorem", "ipsum", "dolor"],
                   ["prisencolinensinainciusol", "alright!"]],
         "structs": [{"foo": 2, "bar": 123}],
         "outer": [{"inner": []},
                   {"inner": [{"foo": 867, "bar": 5309},
                              {"foo": 555, "bar": 1212}]}]},
        {"lists": [], "structs": [], "outer": []}
    ])


def deserialize_struct():
    arg = amp.AmpList([('s', amp.Unicode()),
                       ('b', amp.String())])
    value = [{"s": u"λλλ", "b": "123\x00\xFF"}]
    serialize_to_stdout(arg, value)


def deserialize_nested():
    arg = amp.AmpList([(
        's',
        amp.AmpList([(
            's',
            amp.AmpList([(
                's',
                amp.AmpList([(
                    'v',
                    amp.AmpList([('foo', amp.Unicode()),
                                 ('baz', amp.Unicode())]))]))]))]))])
    value = [{'s':
              [{'s':
                [{'s':
                  [{'v':
                    [{"foo": "Bar", "baz": "Quux"},
                     {"foo": "Bar", "baz": "Quux"}]}]}]}]}]
    serialize_to_stdout(arg, value)


class Echo(amp.Command):
    arguments = [("message", amp.Unicode())]
    response = [("message", amp.Unicode())]


class Ohce(amp.Command):
    arguments = [("message", amp.Unicode())]
    response = [("egassem", amp.Unicode())]


class Quit(amp.Command):
    arguments = []
    response = []


class Mirror(amp.AMP):
    def echo(self, message):
        return {"message": message}
    Echo.responder(echo)

    def ohce(self, message):
        return {"egassem": message[::-1]}
    Ohce.responder(ohce)

    def quit(self):
        reactor.callLater(0.1, reactor.stop)
        return {}
    Quit.responder(quit)


def amp_server():
    pf = Factory()
    pf.protocol = Mirror
    reactor.listenTCP(4242, pf)

    def running():
        print "running"
        sys.stdout.flush()

    reactor.callLater(0, running)
    reactor.callLater(30, reactor.stop)
    reactor.run()


@inlineCallbacks
def setup_connection():
    creator = ClientCreator(reactor, amp.AMP)
    client = yield creator.connectTCP("localhost", 2424)

    d1 = test_echo(client)
    d2 = test_ohce(client)
    DeferredList([d1, d2]).addCallback(lambda _: test_quit(client))


@inlineCallbacks
def test_echo(client):
    result = yield client.callRemote(Echo, message="funkenschlag")
    assert result["message"] == "funkenschlag"


@inlineCallbacks
def test_ohce(client):
    result = yield client.callRemote(Ohce, message="funkenschlag")
    assert result["egassem"] == "funkenschlag"[::-1]


@inlineCallbacks
def test_quit(client):
    result = yield client.callRemote(Quit)
    assert result == {}
    reactor.stop()


def amp_client():
    setup_connection()
    reactor.run()


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    for command, fn in [
        ("serialize_struct", serialize_struct),
        ("serialize_list", serialize_list),
        ("serialize_nested", serialize_nested),
        ("deserialize_struct", deserialize_struct),
        ("deserialize_nested", deserialize_nested),
        ("amp_server", amp_server),
        ("amp_client", amp_client),
    ]:
        subparsers.add_parser(command).set_defaults(func=fn)

    parser.parse_args().func()

if __name__ == "__main__":
    main()
