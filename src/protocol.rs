//! The AMP protocol, which can send or respond to commands given a stream.
//!
//! This module provides the mechanism for setting up an AMP endpoint given a
//! bidirectional stream, to be able to send commands or respond to commands.
//!
//! `SyncEndpoint` is a bidirectional, synchronous, half-duplex endpoint,
//! which allows either waiting for and responding to a command, or sending a
//! command, though only one or the other at a given time.
//!
//! To create `SyncEndpoint`, you simply need a to supply something that
//! implements `Read + Write`, such as a socket:
//!
//! ```no_run
//! use std::io::prelude::*;
//! use std::net::TcpStream;
//! use amp::protocol::SyncEndpoint;
//!
//! let mut stream = TcpStream::connect("localhost:4242").unwrap();
//! let mut endpoint = SyncEndpoint::new(stream);
//! ```
//!
//! Once you have a stream, you can execute commands by invoking `call`, with
//! the command and a value of the argument type of the command, which will
//! return a result that will either be the result type of the command, or an
//! error type indicating either one of the error types defined for the
//! command, or a protocol error.
//!
//! Following the example from [`amp::command`]:
//!
//! ```no_run
//! # #![feature(custom_derive, plugin)]
//! # #![plugin(serde_macros)]
//! #
//! # #[macro_use]
//! # extern crate amp;
//! #
//! # fn main() {
//! # use std::io::prelude::*;
//! # use std::net::TcpStream;
//! # use amp::protocol::SyncEndpoint;
//! #
//! # command! {
//! #     struct SendMessage {
//! #         arguments: SendArgs {
//! #             title: String,
//! #             recipients: Vec<String>,
//! #             body: String
//! #         },
//! #         response: SendResponse {
//! #             success: Vec<bool>
//! #         },
//! #         errors: SendError {
//! #             INVALID_RECIPIENT,
//! #             SERVER_DOWN,
//! #             MESSAGE_TOO_LARGE,
//! #             ..UNKNOWN
//! #         }
//! #     }
//! # }
//! # let mut stream = TcpStream::connect("localhost:4242").unwrap();
//! # let mut endpoint = SyncEndpoint::new(stream);
//! #
//! use amp::protocol::ProtocolError::*;
//!
//! match endpoint.call(
//!     SendMessage,
//!     SendArgs {
//!         title: "Hello!".into(),
//!         recipients: vec!["a@example.com".into(), "b@example.com".into()],
//!         body: "Hi all!".into(),
//!     }) {
//!     Ok(_result) => println!("Sent!"),
//!     Err(Command(SendError::INVALID_RECIPIENT(message))) =>
//!         println!("Invalid recipient: {}", message),
//!     Err(Command(SendError::SERVER_DOWN(message))) =>
//!         println!("Server down: {}", message),
//!     err @ _ => println!("Unexpected error: {:?}", err),
//! }
//! # }
//! ```
//!
//! Instead of calling a command, you can receive commands on an endpoint.  To
//! do so, you need to register a `SyncResponder` for each command that you
//! can handle.  A responder is something that takes an `AmpBox`, or hashmap
//! from strings to byte strings, as an argument, and returns a
//! `Result<AmpBox, AmpBox>`.  The `SyncResponder` trait is implemented for
//! pairs of a `Command` and a closure that takes the appropriate arguments
//! and returns the appropriate results, so such closures can be registered
//! and have their arguments and results marshalled and demarshalled
//! automatically.
//!
//! ```no_run
//! # #![feature(custom_derive, plugin)]
//! # #![plugin(serde_macros)]
//! #
//! # #[macro_use]
//! # extern crate amp;
//! #
//! # fn main() {
//! # use std::io::prelude::*;
//! # use std::net::TcpStream;
//! # use amp::protocol::SyncEndpoint;
//! #
//! # command! {
//! #     struct SendMessage {
//! #         arguments: SendArgs {
//! #             title: String,
//! #             recipients: Vec<String>,
//! #             body: String
//! #         },
//! #         response: SendResponse {
//! #             success: Vec<bool>
//! #         },
//! #         errors: SendError {
//! #             INVALID_RECIPIENT,
//! #             SERVER_DOWN,
//! #             MESSAGE_TOO_LARGE,
//! #             ..UNKNOWN
//! #         }
//! #     }
//! # }
//! # let stream = TcpStream::connect("localhost:4242").unwrap();
//! # let mut endpoint = SyncEndpoint::new(stream);
//! #
//! fn send_inner(_title: &str, _recipients: &[String], _body: &str)
//!     -> Result<SendResponse, SendError>
//! {
//!     Ok(SendResponse { success: vec![] })
//! }
//!
//! endpoint.register(
//!     SendMessage,
//!     |args: SendArgs| send_inner(&args.title, &args.recipients, &args.body)
//! )
//! # }
//! ```
//!
//! Once all necessary commands are registered, wait for and process one
//! command with:
//!
//! ```no_run
//! # use std::io::prelude::*;
//! # use std::net::TcpStream;
//! # use amp::protocol::SyncEndpoint;
//! #
//! # let mut stream = TcpStream::connect("localhost:4242").unwrap();
//! # let mut endpoint = SyncEndpoint::new(stream);
//! #
//! let _ = endpoint.respond_once();
//! ```
//!
//! If you are simply implementing a server that responds to commands, put that
//! in a loop; if you want to interleave responding and sending commands, you
//! may do that as well.
//!
//! [`amp::command`]: ../command/index.html

use std::io::prelude::*;
use std::error::Error;
use std::collections::HashMap;

use rand::random;
use data_encoding::hex;
use serde_crate::bytes::ByteBuf;

use command::{self, Command};
use command::Error as CommandError;
use serde::{self, AmpBox};

const ASK: &'static str = "_ask";
const COMMAND: &'static str = "_command";
const ANSWER: &'static str = "_answer";
const ERROR: &'static str = "_error";
const ERROR_CODE: &'static str = "_error_code";
const ERROR_DESCRIPTION: &'static str = "_error_description";
const UNHANDLED: &'static str = "UNHANDLED";

/// Handler for a command, which can take an `AmpBox` that has already been
/// deserialized at the top level, perform the appropriate action, and return
/// a `Result<AmpBox, AmpBox>` representing either success or failure.
pub trait SyncResponder {
    fn respond(&mut self, message: AmpBox) -> Result<AmpBox, AmpBox>;
}

impl From<serde::Error> for AmpBox {
    fn from(err: serde::Error) -> Self {
        let mut amp_box = AmpBox::new();
        amp_box.insert(ERROR_CODE.into(),
                       command::UNKNOWN_ERROR.to_owned().into_bytes().into());
        amp_box.insert(ERROR_DESCRIPTION.into(),
                       err.description().to_owned().into_bytes().into());
        amp_box
    }
}

impl From<serde::DeserializerError> for AmpBox {
    fn from(err: serde::DeserializerError) -> Self {
        let mut amp_box = AmpBox::new();
        amp_box.insert(ERROR_CODE.into(),
                       command::UNKNOWN_ERROR.to_owned().into_bytes().into());
        amp_box.insert(ERROR_DESCRIPTION.into(),
                       err.description().to_owned().into_bytes().into());
        amp_box
    }
}

fn amp_error<C, D>(code: C, description: D) -> AmpBox
    where C: Into<Vec<u8>>,
          D: Into<Vec<u8>>,
{
    let mut err = AmpBox::new();
    let code_vec: Vec<u8> = code.into();
    let description_vec: Vec<u8> = description.into();
    err.insert(ERROR_CODE.into(), code_vec.into());
    err.insert(ERROR_DESCRIPTION.into(), description_vec.into());
    err
}

/// Represents a pair of a command, and a function which can respond to that
/// command, meaning that it takes as arguments the arguments structure of the
/// command, and returns a result corresponding to the command response or
/// command error types.
pub struct CommandResponder<C, F>(C, F);

impl<C, F> SyncResponder for CommandResponder<C, F>
    where C: Command,
          F: FnMut(C::Argument) -> Result<C::Response, C::Error>,
{
    fn respond(&mut self, message: AmpBox) -> Result<AmpBox, AmpBox> {
        let arg: C::Argument = try!(serde::from_box(&message));
        match self.1(arg) {
            Ok(res) => Ok(try!(serde::to_box(&res))),
            Err(err) => {
                let (code, desc) = err.to_code_description();
                Err(amp_error(code, desc))
            }
        }
    }
}

pub struct SyncEndpoint<'a, S> {
    stream: S,
    responders: HashMap<String, Box<SyncResponder + 'a>>,
}

/// The error type that `SyncEndpoint::call` can return.  May be an error
/// defined by the `Command` interface representing an error return from the
/// other side of the connection; or may represent some form of protocol
/// error.
#[derive(Debug)]
pub enum ProtocolError<E> {
    /// An error returned from the other side of the connection, of the type
    /// defined by the `Command`
    Command(E),
    /// An error serializing the request onto the stream.
    Serialization(serde::Error),
    /// An error deserializing the response from the stream.
    Deserializion(serde::DeserializerError),
    /// Received a reply that didn't correspond to the request.
    UnexpectedReply,
}

impl<E> From<E> for ProtocolError<E>
    where E: command::Error,
{
    fn from(err: E) -> Self {
        ProtocolError::Command(err)
    }
}

impl<E> From<serde::Error> for ProtocolError<E> {
    fn from(err: serde::Error) -> Self {
        ProtocolError::Serialization(err)
    }
}

impl<E> From<serde::DeserializerError> for ProtocolError<E> {
    fn from(err: serde::DeserializerError) -> Self {
        ProtocolError::Deserializion(err)
    }
}

/// The error type that `SyncEndpoint::respond_once` can return.
#[derive(Debug)]
pub enum RespondError {
    Serialization(serde::Error),
    Deserializion(serde::DeserializerError),
    MissingCommand,
}

impl From<serde::Error> for RespondError {
    fn from(err: serde::Error) -> Self {
        RespondError::Serialization(err)
    }
}

impl From<serde::DeserializerError> for RespondError {
    fn from(err: serde::DeserializerError) -> Self {
        RespondError::Deserializion(err)
    }
}

impl<'a, S> SyncEndpoint<'a, S>
    where S: Read + Write,
{
    /// Create a `SyncEndpoint` that will read from and write to the given stream.
    pub fn new(stream: S) -> Self {
        SyncEndpoint {
            stream: stream,
            responders: HashMap::new(),
        }
    }
    /// Call the given command on the remote side, passing in the given
    /// argument of the type specified by the command.  Returns a result
    /// of either the response type specified by the command, or an error.
    /// The error may contain a `Command` value containing the error type
    /// specified by the command, or one of several protocol errors that
    /// indicates a communication problem rather than an issue with the
    /// remote command.
    pub fn call<C>(
        &mut self,
        command: C,
        args: C::Argument
    ) -> Result<C::Response, ProtocolError<C::Error>>
        where C: Command,
    {
        let id = random::<[u8; 32]>();
        let id = hex::encode(&id[..]);
        let mut amp_box = try!(serde::to_box(&args));
        amp_box.insert(ASK.into(), try!(serde::to_vec(&id)).into());
        amp_box.insert(COMMAND.into(),
                       try!(serde::to_vec(&command.name())).into());
        try!(serde::to_writer(&mut self.stream, &amp_box));
        let mut response: AmpBox = try!(serde::from_reader(&mut self.stream));
        let answer = response.remove(ANSWER);
        match answer {
            Some(ref reply) if id ==
                               try!(serde::from_slice::<String>(&reply)) => {
                Ok(try!(serde::from_box(&response)))
            }
            Some(_) | None => Err(ProtocolError::UnexpectedReply),
        }
    }
    /// Register the given responder for the given command.
    pub fn register<C: 'a, F: 'a>(&mut self, command: C, func: F)
        where C: Command,
              CommandResponder<C, F>: SyncResponder,
    {
        self.responders.insert(command.name().into(),
                               Box::new(CommandResponder(command, func)));
    }
    fn run_command(
        &mut self,
        command: ByteBuf,
        args: AmpBox
    ) -> Result<AmpBox, AmpBox> {
        let command_str: String = match serde::from_slice(&command) {
            Ok(s) => s,
            Err(_) => {
                return Err(amp_error(UNHANDLED,
                                     "Cannot handle request, \
                                      command not a valid UTF-8 string"))
            }
        };
        match self.responders.get_mut(&command_str) {
            Some(responder) => responder.respond(args),
            None => {
                Err(amp_error(UNHANDLED,
                              format!("Unhandled Command: '{}'",
                                      command_str)))
            }
        }
    }
    /// Wait to receive a command, and respond to it synchronously.  Currently
    /// does not return any errors, this should be fixed to allow closing the
    /// connection, logging, or doing something else in response to errors.
    pub fn respond_once(&mut self) -> Result<(), RespondError> {
        let mut incoming: AmpBox = try!(serde::from_reader(&mut self.stream));
        let ask = incoming.remove(ASK);
        let command = incoming.remove(COMMAND);
        match (ask, command) {
            (Some(token), Some(command)) => {
                let result = self.run_command(command, incoming);
                let response = match result {
                    Ok(mut answer) => {
                        answer.insert(ANSWER.into(), token);
                        answer
                    }
                    Err(mut err) => {
                        err.insert(ERROR.into(), token);
                        err
                    }
                };
                try!(serde::to_writer(&mut self.stream, &response));
                Ok(())
            }
            (None, Some(command)) => {
                // This case is valid in AMP, indicating a fire-and-forget command
                let _ = self.run_command(command, incoming);
                Ok(())
            }
            (Some(token), None) => {
                // This error does have a token, so we can both send an error
                // to the other side, as well as returning an error from
                // respond_once.
                let mut result = amp_error(
                    UNHANDLED,
                    "Cannot handle request, missing command"
                );
                result.insert(ERROR.into(), token);
                try!(serde::to_writer(&mut self.stream, &result));

                Err(RespondError::MissingCommand)
            }
            (None, None) => Err(RespondError::MissingCommand),
        }
    }
}

#[cfg(test)]
mod test {
    use std::io::prelude::*;
    use std::io;

    use super::SyncEndpoint;
    use util;

    struct Stream<R, W> {
        read: R,
        write: W,
    }

    impl<R: Read, W> Read for Stream<R, W> {
        fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
            self.read.read(buf)
        }
    }

    impl<R, W: Write> Write for Stream<R, W> {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.write.write(buf)
        }
        fn flush(&mut self) -> io::Result<()> {
            self.write.flush()
        }
    }

    #[test]
    fn pipe_protocol() {
        command! {
            struct Query {
                arguments: QueryArgs { query: String, results: u32 },
                response: QueryRes { results: Vec<String> },
                errors: QueryErr { QUERY_INVALID, NOT_FOUND, ..UNKNOWN }
            }
        }

        let (server_read, client_write) = util::pipe();
        let (client_read, server_write) = util::pipe();

        let client_stream = Stream {
            read: client_read,
            write: client_write,
        };
        let server_stream = Stream {
            read: server_read,
            write: server_write,
        };

        let guard = ::std::thread::spawn(move || {
            let mut server_endpoint = SyncEndpoint::new(server_stream);
            server_endpoint.register(Query, |_args: QueryArgs| {
                Ok(QueryRes { results: vec!["Response".into()] })
            });
            assert!(match server_endpoint.respond_once() {
                Ok(()) => true,
                _ => false,
            });
            assert!(match server_endpoint.respond_once() {
                Ok(()) => true,
                _ => false,
            });
        });

        let mut client_endpoint = SyncEndpoint::new(client_stream);

        let res = client_endpoint.call(
            Query,
            QueryArgs {
                query: "foobar".into(),
                results: 5,
            }
        ).unwrap();
        assert_eq!(res.results[0], "Response");

        let res = client_endpoint.call(
            Query,
            QueryArgs {
                query: "loremipsum".into(),
                results: 10,
            }
        ).unwrap();
        assert_eq!(res.results[0], "Response");

        guard.join().unwrap();
    }
}
