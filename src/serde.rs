//! Functions for serializing and deserializing objects that implement the
//! `serde::Serialize` and `serde::Deserialize` traits into the format used by
//! the [AMP protocol].
//!
//! [AMP protocol]: https://amp-protocol.net/
//!
//! # Examples
//!
//! ```
//! #![feature(plugin, custom_derive)]
//! #![plugin(serde_macros)]
//!
//! extern crate serde;
//! extern crate amp;
//!
//! use serde::{Serialize, Deserialize};
//! use amp::serde::{to_vec, from_slice};
//!
//! # fn main() {
//! #[derive(Serialize, Deserialize, PartialEq, Debug)]
//! struct Person {
//!     name: String,
//!     age: u8,
//!     height: f64,
//! }
//!
//! let bob = Person { name: "Bob".into(), age: 37, height: 1.75 };
//! let serialized = to_vec(&bob).unwrap();
//! let deserialized: Person = from_slice(&serialized).unwrap();
//! assert_eq!(bob, deserialized);
//! # }
//! ```

use std::{error, fmt, io, iter, num, str, string, u8, u16};
use std::collections::{hash_map, HashMap};
use std::io::{Write, Read};

use serde_crate::{Serializer, Deserializer, Serialize, Deserialize};
use serde_crate::bytes::ByteBuf;
use byteorder::{NetworkEndian, WriteBytesExt, ReadBytesExt};

// Hack to work around path issue in which macros try to import from
// serde::ser and and serde::de.
#[doc(hidden)]
pub use serde_crate::{de, ser};

/// Represents the top-level message structure in an AMP message, with the
/// values of each of the keys serialized.  Used to hold a message in which
/// the key-value pairs have been parsed out, but because the key indicating
/// which command or response this is may not be at the beginning of the
/// message, we don't yet know how to parse the values.
pub type AmpBox = HashMap<String, ByteBuf>;

#[derive(Debug)]
pub enum Error {
    // The following errors are private, and should be translated into the
    // appropriate public errors as they are returned.
    #[doc(hidden)]
    KeySliceTooLong,
    #[doc(hidden)]
    ValueSliceTooLong,
    #[doc(hidden)]
    ValueStringTooLong(String),
    /// The key is too long to be encoded in AMP, which limits keys to at most
    /// 255 bytes of UTF-8
    KeyTooLong(String),
    /// A value was too long to be encoded in AMP.  The value of any key must
    /// be no longer than 65535 bytes when encoded
    ValueTooLong(String, Option<String>),
    /// Tried to serialize something other than a map into an `AmpBox`
    AmpBoxUnserializable,
    /// There was an IO error while writing
    Io(io::Error),
    /// The `Serialize` implementation raised a custom error
    Custom(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        // FIXME - implement actual Display, not using Debug
        write!(f, "{:?}", &self)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::KeySliceTooLong |
            Error::KeyTooLong(_) => "key longer than maximum 255 bytes",
            Error::ValueSliceTooLong |
            Error::ValueStringTooLong(_) |
            Error::ValueTooLong(_, _) => {
                "value longer than maximum 65535 bytes"
            }
            Error::AmpBoxUnserializable => {
                "tried to serialize something that isn't a map"
            }
            Error::Io(ref err) => err.description(),
            Error::Custom(ref s) => s,
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<string::FromUtf8Error> for Error {
    fn from(_err: string::FromUtf8Error) -> Error {
        Error::Custom("could not convert to UTF-8 while trying to format \
                       error"
            .into())
    }
}

impl ser::Error for Error {
    fn custom<T: Into<String>>(msg: T) -> Self {
        Error::Custom(msg.into())
    }
}

fn add_key_to_value_error<K, V>(key: K, _val: V, error: Error) -> Error
    where K: Serialize,
          V: Serialize,
{
    let mut buf = Vec::new();
    {
        let mut key_serializer = AmpSerializer::new(&mut buf);
        if let Err(err) = key.serialize(&mut key_serializer) {
            return err;
        }
    }
    let key_string = match String::from_utf8(buf) {
        Ok(string) => string,
        Err(err) => return err.into(),
    };
    match error {
        Error::ValueSliceTooLong => Error::ValueTooLong(key_string, None),
        Error::ValueStringTooLong(val_string) => {
            Error::ValueTooLong(key_string, Some(val_string))
        }
        _ => error,
    }
}


/// Represents the outer object we are serializing into.
///
/// We use this in our serializer and deserializer to keep track of whether we
/// need to write or read an object length, and what limit that length should
/// have.
///
/// When serializing an object, we need to write out a byte length of the
/// object; but when walking down the object graph visiting objects, we don't
/// know how long an object is in bytes until we have traversed down to it.
/// For this reason, we need to write the length once we're serializing a
/// particular object and know its length.  But, in the AMP serialization, we
/// don't write the length before serializing a map within a sequence, since
/// a sequence of maps (called `AmpList`) is delimited by a terminator rather
/// than prefixed with a length.
///
/// A similar pattern occurs when deserializing, where we need to know when
/// we should and should't be reading a length based on the type.
#[derive(Copy, Clone, Eq, PartialEq)]
enum Context {
    None,
    Seq,
    MapKey,
    MapVal,
}

struct AmpSerializer<'a, W: 'a> {
    writer: &'a mut W,
    context: Context,
}

impl<'a, W: io::Write> AmpSerializer<'a, W> {
    fn new(writer: &'a mut W) -> Self {
        AmpSerializer {
            writer: writer,
            context: Context::None,
        }
    }

    fn context(mut self, context: Context) -> Self {
        self.context = context;
        self
    }

    fn write_bytes(&mut self, value: &[u8]) -> Result<(), Error> {
        let len = value.len();
        if self.context == Context::MapKey && len > u8::MAX as usize {
            return Err(Error::KeySliceTooLong);
        } else if self.context != Context::None {
            if len > u16::MAX as usize {
                return Err(Error::ValueSliceTooLong);
            } else {
                try!(self.writer.write_u16::<NetworkEndian>(len as u16));
            }
        }
        try!(self.writer.write(value));
        Ok(())
    }

    fn serialize_display<T>(&mut self, v: T) -> Result<(), Error>
        where T: fmt::Display,
    {
        let mut buf = Vec::new();
        try!(write!(&mut buf, "{}", v));
        self.write_bytes(&buf)
    }
}

impl<'a, W: io::Write> Serializer for AmpSerializer<'a, W> {
    type Error = Error;

    fn serialize_bool(&mut self, v: bool) -> Result<(), Self::Error> {
        if v {
            self.serialize_display("True")
        } else {
            self.serialize_display("False")
        }
    }
    fn serialize_i64(&mut self, v: i64) -> Result<(), Self::Error> {
        self.serialize_display(v)
    }
    fn serialize_u64(&mut self, v: u64) -> Result<(), Self::Error> {
        self.serialize_display(v)
    }
    fn serialize_f64(&mut self, v: f64) -> Result<(), Self::Error> {
        self.serialize_display(v)
    }
    fn serialize_bytes(&mut self, value: &[u8]) -> Result<(), Self::Error> {
        self.write_bytes(value)
    }
    fn serialize_str(&mut self, value: &str) -> Result<(), Self::Error> {
        let bytes = value.as_bytes();
        self.serialize_bytes(bytes).map_err(|e| {
            match e {
                Error::KeySliceTooLong => Error::KeyTooLong(value.into()),
                Error::ValueSliceTooLong => {
                    Error::ValueStringTooLong(value.into())
                }
                _ => e,
            }
        })
    }
    fn serialize_unit(&mut self) -> Result<(), Self::Error> {
        unimplemented!();
    }
    fn serialize_none(&mut self) -> Result<(), Self::Error> {
        unimplemented!();
    }
    fn serialize_some<V>(&mut self, _value: V) -> Result<(), Self::Error>
        where V: Serialize,
    {
        unimplemented!();
    }
    fn serialize_seq<V>(&mut self, mut visitor: V) -> Result<(), Self::Error>
        where V: ser::SeqVisitor,
    {
        let mut buf = Vec::new();
        {
            let mut seq_serializer = AmpSerializer::new(&mut buf)
                .context(Context::Seq);
            while let Some(()) = try!(visitor.visit(&mut seq_serializer)) {
            }
        }
        self.write_bytes(&buf)
    }
    fn serialize_seq_elt<T>(&mut self, value: T) -> Result<(), Self::Error>
        where T: Serialize,
    {
        value.serialize(self)
    }
    fn serialize_map<V>(&mut self, mut visitor: V) -> Result<(), Self::Error>
        where V: ser::MapVisitor,
    {
        if self.context == Context::Seq || self.context == Context::None {
            while let Some(()) = try!(visitor.visit(self)) {}
            try!(self.writer.write_u16::<NetworkEndian>(0 as u16));
        } else {
            let mut buf = Vec::new();
            {
                let mut map_ser = AmpSerializer::new(&mut buf);
                while let Some(()) = try!(visitor.visit(&mut map_ser)) {}
                try!(map_ser.writer.write_u16::<NetworkEndian>(0 as u16));
            }
            try!(self.write_bytes(&buf));
        }
        Ok(())
    }
    fn serialize_map_elt<K, V>(
        &mut self,
        key: K,
        value: V
    ) -> Result<(), Self::Error>
        where K: Serialize,
              V: Serialize,
    {
        {
            let mut key_serializer = AmpSerializer::new(&mut self.writer)
                .context(Context::MapKey);
            try!(key.serialize(&mut key_serializer));
        }
        let mut val_serializer = AmpSerializer::new(&mut self.writer)
            .context(Context::MapVal);
        value.serialize(&mut val_serializer)
            .map_err(|e| add_key_to_value_error(key, value, e))
    }
}

/// A serializer that serializes an object into an `AmpBox`, or map from
/// strings to byte vectors, as an intermediate step before serializing it
/// onto the wire.  For this reason, we can only serialize objects that
/// serialize as maps; the keys are serialized as `AmpBox` keys, and the
/// values fully serialized into `ByteBuf`.  Serializing a map consists of
/// serializing the key, serializing the value, and then inserting the given
/// key and value into the `AmpBox` map.  This then allows you to just
/// serialize that `AmpBox` onto the wire to get the final representation.
pub struct AmpBoxSerializer(AmpBox);

impl Serializer for AmpBoxSerializer {
    type Error = Error;
    fn serialize_bool(&mut self, _v: bool) -> Result<(), Self::Error> {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_i64(&mut self, _v: i64) -> Result<(), Self::Error> {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_u64(&mut self, _v: u64) -> Result<(), Self::Error> {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_f64(&mut self, _v: f64) -> Result<(), Self::Error> {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_str(&mut self, _value: &str) -> Result<(), Self::Error> {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_unit(&mut self) -> Result<(), Self::Error> {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_none(&mut self) -> Result<(), Self::Error> {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_some<V>(&mut self, _value: V) -> Result<(), Self::Error>
        where V: Serialize,
    {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_seq<V>(&mut self, _visitor: V) -> Result<(), Self::Error>
        where V: ser::SeqVisitor,
    {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_seq_elt<T>(&mut self, _value: T) -> Result<(), Self::Error>
        where T: Serialize,
    {
        Err(Error::AmpBoxUnserializable)
    }
    fn serialize_map<V>(&mut self, mut visitor: V) -> Result<(), Self::Error>
        where V: ser::MapVisitor,
    {
        while let Some(()) = try!(visitor.visit(self)) {}
        Ok(())
    }
    fn serialize_map_elt<K, V>(
        &mut self,
        key: K,
        value: V
    ) -> Result<(), Self::Error>
        where K: Serialize,
              V: Serialize,
    {
        // Use the default Context::None for these serializers, since we don't
        // need the length in the key or value; the lengths will be added when
        // we re-serialize this AmpBox onto the wire.

        let mut serialized_key = Vec::new();
        {
            let mut key_serializer = AmpSerializer::new(&mut serialized_key);
            try!(key.serialize(&mut key_serializer));
        }
        let string_key = try!(String::from_utf8(serialized_key));

        let mut serialized_val = Vec::new();
        {
            let mut val_serializer = AmpSerializer::new(&mut serialized_val);
            try!(value.serialize(&mut val_serializer)
                .map_err(|e| add_key_to_value_error(key, value, e)));
        }

        self.0.insert(string_key, serialized_val.into());

        Ok(())
    }
}

/// Serialize the given value to a writer implementing `std::io::Write`
pub fn to_writer<W, T>(writer: &mut W, value: &T) -> Result<(), Error>
    where W: io::Write,
          T: Serialize,
{
    let mut ser = AmpSerializer::new(writer);
    try!(value.serialize(&mut ser));
    Ok(())
}

/// Serialize the given value to a `Vec<u8>`, returning the result
pub fn to_vec<T>(value: &T) -> Result<Vec<u8>, Error>
    where T: Serialize,
{
    let mut vec = Vec::new();
    try!(to_writer(&mut vec, value));
    Ok(vec)
}

/// Serialize the given value, which must serialize as a map type with strings
/// as keys, to an `AmpBox`, returning the result.  The keys will be the keys
/// of the map, the values will be the serialized values of the map.
pub fn to_box<T>(value: &T) -> Result<AmpBox, Error>
    where T: Serialize,
{
    let mut serializer = AmpBoxSerializer(AmpBox::new());
    try!(value.serialize(&mut serializer));
    Ok(serializer.0)
}

#[derive(Debug)]
pub enum DeserializerError {
    /// Serializing this type is unsupported
    Unsupported,
    /// When reading the key, we got a length longer than 255 bytes
    KeyTooLong(u16),
    /// When reading a struct, there were still keys left in the box after
    /// reading all keys
    TooManyKeys,
    /// When reading a fixed length sequence, there were still items left
    /// after reading the required number
    UnexpectedSeqEnd,
    /// A boolean was not serialized as either `True` or `False`
    InvalidBool(Vec<u8>),
    /// There was an error reading the required number of bytes
    Io(io::Error),
    /// The `Deserialize` implementation raised a custom error
    Custom(String),
    /// Serde raised an error
    Serde(de::value::Error),
    /// A string was not valid UTF-8
    InvalidEncoding(str::Utf8Error),
    /// There was an error parsing an integer value
    ParseIntError(num::ParseIntError),
    /// There was an error parsing a floating point value
    ParseFloatError(num::ParseFloatError),
}

impl fmt::Display for DeserializerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        // FIXME - implement actual Display, not using Debug
        write!(f, "{:?}", &self)
    }
}

impl error::Error for DeserializerError {
    fn description(&self) -> &str {
        match *self {
            DeserializerError::Unsupported => {
                "deserializing as this type is not supported"
            }
            DeserializerError::KeyTooLong(_) => {
                "key is longer than 255 bytes"
            }
            DeserializerError::TooManyKeys => {
                "box had more keys than expected"
            }
            DeserializerError::UnexpectedSeqEnd => {
                "more values in sequence than expected"
            }
            DeserializerError::InvalidBool(_) => {
                "boolean was not False or True"
            }
            DeserializerError::Io(ref err) => err.description(),
            DeserializerError::Custom(ref s) => s,
            DeserializerError::Serde(ref err) => err.description(),
            DeserializerError::InvalidEncoding(ref err) => err.description(),
            DeserializerError::ParseIntError(ref err) => err.description(),
            DeserializerError::ParseFloatError(ref err) => err.description(),
        }
    }
}

impl From<io::Error> for DeserializerError {
    fn from(err: io::Error) -> Self {
        DeserializerError::Io(err)
    }
}

impl From<str::Utf8Error> for DeserializerError {
    fn from(err: str::Utf8Error) -> Self {
        DeserializerError::InvalidEncoding(err)
    }
}

impl From<num::ParseIntError> for DeserializerError {
    fn from(err: num::ParseIntError) -> Self {
        DeserializerError::ParseIntError(err)
    }
}

impl From<num::ParseFloatError> for DeserializerError {
    fn from(err: num::ParseFloatError) -> Self {
        DeserializerError::ParseFloatError(err)
    }
}

impl de::Error for DeserializerError {
    fn custom<T: Into<String>>(desc: T) -> Self {
        DeserializerError::Serde(de::value::Error::Custom(desc.into()))
    }

    fn end_of_stream() -> Self {
        DeserializerError::Serde(de::value::Error::EndOfStream)
    }
}

pub type DeserializerResult<T> = Result<T, DeserializerError>;

/// Reader that can wrap an underlying reader; it will either be Unlimited,
/// in which it will just forward to the underlying reader, or Limited,
/// in which case it will use `std::io::Take` to limit the reader to read
/// a certain number of bytes.  This can be used both as an assertion to
/// prevent us from reading past the end of a field, and also so when
/// reading a `ListOf` or `AmpList`, we know when we've reached the end.
enum AmpReader<'a, R: 'a> {
    Unlimited(&'a mut R),
    Limited(io::Take<&'a mut R>),
}

impl<'a, R> Read for AmpReader<'a, R>
    where R: Read,
{
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match *self {
            AmpReader::Unlimited(ref mut r) => r.read(buf),
            AmpReader::Limited(ref mut r) => r.read(buf),
        }
    }
}

struct AmpDeserializer<'a, R: 'a> {
    reader: AmpReader<'a, R>,
    context: Context,
    map_end: bool,
}

impl<'a, R: Read> AmpDeserializer<'a, R> {
    fn new(reader: AmpReader<'a, R>) -> Self {
        AmpDeserializer {
            reader: reader,
            context: Context::None,
            map_end: false,
        }
    }

    fn context(mut self, context: Context) -> Self {
        self.context = context;
        self
    }

    fn maybe_read_length<'b>
        (&'b mut self)
         -> DeserializerResult<AmpReader<'b, AmpReader<'a, R>>> {
        match (self.context, &mut self.reader) {
            (Context::None, reader @ &mut AmpReader::Unlimited(_)) => {
                Ok(AmpReader::Unlimited(reader))
            }
            (Context::None, reader @ &mut AmpReader::Limited(_)) => {
                let len = match *reader {
                    AmpReader::Limited(ref l) => l.limit(),
                    _ => unreachable!(),
                };
                let limited = reader.take(len);
                Ok(AmpReader::Limited(limited))
            }
            (_, reader) => {
                let len = try!(reader.read_u16::<NetworkEndian>()) as u64;
                if self.context == Context::MapKey {
                    if len == 0 {
                        self.map_end = true;
                    } else if len > u8::MAX as u64 {
                        return Err(DeserializerError::KeyTooLong(len as u16));
                    }
                }
                let limited = reader.take(len);
                Ok(AmpReader::Limited(limited))
            }
        }
    }

    fn read_atomic_value(&mut self) -> DeserializerResult<Vec<u8>> {
        let mut reader = try!(self.maybe_read_length());
        let mut buf = Vec::new();
        try!(reader.read_to_end(&mut buf));
        Ok(buf)
    }
}

impl<'a, R: Read> Deserializer for AmpDeserializer<'a, R> {
    type Error = DeserializerError;

    fn deserialize<V>(&mut self, _visitor: V) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        unimplemented!();
        // TODO: should return Err(DeserializerError::Unsupported), but
        // unimplemented panic is better for now as it shows me which other
        // methods are unimplemented that are falling back on this.
    }

    fn deserialize_i64<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        let buf = try!(self.read_atomic_value());
        let str = try!(str::from_utf8(&buf));
        visitor.visit_i64(try!(str.parse::<i64>()))
    }

    fn deserialize_u64<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        let buf = try!(self.read_atomic_value());
        let str = try!(str::from_utf8(&buf));
        visitor.visit_u64(try!(str.parse::<u64>()))
    }

    fn deserialize_f64<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        let buf = try!(self.read_atomic_value());
        let str = try!(str::from_utf8(&buf));
        visitor.visit_f64(try!(str.parse::<f64>()))
    }

    fn deserialize_bool<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        let buf = try!(self.read_atomic_value());
        if buf == b"True" {
            visitor.visit_bool(true)
        } else if buf == b"False" {
            visitor.visit_bool(false)
        } else {
            Err(DeserializerError::InvalidBool(buf))
        }
    }

    fn deserialize_struct_field<V>(
        &mut self,
        visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        self.deserialize_string(visitor)
    }

    fn deserialize_string<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        let buf = try!(self.read_atomic_value());
        let s = try!(String::from_utf8(buf).map_err(|err| err.utf8_error()));
        visitor.visit_string(s)
    }

    fn deserialize_bytes<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        let buf = try!(self.read_atomic_value());
        visitor.visit_byte_buf(buf)
    }

    fn deserialize_seq<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        struct SeqVisitor<'a, R: 'a> {
            de: &'a mut AmpDeserializer<'a, R>,
        }

        impl<'a, R: Read> de::SeqVisitor for SeqVisitor<'a, R> {
            type Error = DeserializerError;

            fn visit<T>(&mut self) -> Result<Option<T>, Self::Error>
                where T: de::Deserialize,
            {
                match self.de.reader {
                    AmpReader::Unlimited(_) => {
                        panic!("cannot parse an unlimited seq")
                    }
                    AmpReader::Limited(ref reader) if reader.limit() == 0 => {
                        Ok(None)
                    }
                    _ => Ok(Some(try!(Deserialize::deserialize(self.de)))),
                }
            }

            fn end(&mut self) -> Result<(), Self::Error> {
                match self.de.reader {
                    AmpReader::Unlimited(_) => {
                        panic!("cannot parse an unlimited seq")
                    }
                    AmpReader::Limited(ref reader) if reader.limit() == 0 => {
                        Ok(())
                    }
                    _ => Err(DeserializerError::UnexpectedSeqEnd),
                }
            }
        }

        let reader = try!(self.maybe_read_length());
        let mut de = AmpDeserializer::new(reader).context(Context::Seq);
        visitor.visit_seq(SeqVisitor { de: &mut de })
    }

    fn deserialize_map<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        struct MapVisitor<'a, R: 'a> {
            de: &'a mut AmpDeserializer<'a, R>,
            read_end: bool,
        }

        impl<'a, R: Read> de::MapVisitor for MapVisitor<'a, R> {
            type Error = DeserializerError;

            fn visit_key<K>(&mut self) -> Result<Option<K>, Self::Error>
                where K: de::Deserialize,
            {
                let box_reader = AmpReader::Unlimited(&mut self.de.reader);
                let mut de = AmpDeserializer::new(box_reader)
                    .context(Context::MapKey);
                let val = try!(Deserialize::deserialize(&mut de));
                if de.map_end {
                    self.read_end = true;
                    Ok(None)
                } else {
                    Ok(Some(val))
                }
            }

            fn visit_value<V>(&mut self) -> Result<V, Self::Error>
                where V: de::Deserialize,
            {
                let box_reader = AmpReader::Unlimited(&mut self.de.reader);
                let mut de = AmpDeserializer::new(box_reader)
                    .context(Context::MapVal);
                Deserialize::deserialize(&mut de)
            }

            fn end(&mut self) -> Result<(), Self::Error> {
                if self.read_end {
                    Ok(())
                } else {
                    let len =
                        try!(self.de.reader.read_u16::<NetworkEndian>()) as usize;
                    if len == 0 {
                        Ok(())
                    } else {
                        Err(DeserializerError::TooManyKeys)
                    }
                }
            }
        }

        let reader = if self.context != Context::None &&
                        self.context != Context::Seq {
            try!(self.maybe_read_length())
        } else {
            AmpReader::Unlimited(&mut self.reader)
        };
        let mut de = AmpDeserializer::new(reader).context(Context::Seq);
        visitor.visit_map(MapVisitor {
            de: &mut de,
            read_end: false,
        })
    }
}

/// A deserializer which deserializes out of an `AmpBox`, or map from `String`
/// to `ByteBuf`, rather than from an actual serial stream.  Can only
/// deserialize map values (including structs and other things represented by
/// serde as a map).
struct AmpBoxDeserializer<'a>(&'a AmpBox);

impl<'a> Deserializer for AmpBoxDeserializer<'a> {
    type Error = DeserializerError;

    fn deserialize<V>(&mut self, _visitor: V) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        Err(DeserializerError::Unsupported)
    }

    fn deserialize_map<V>(
        &mut self,
        mut visitor: V
    ) -> DeserializerResult<V::Value>
        where V: de::Visitor,
    {
        struct MapVisitor<'a> {
            iter: iter::Peekable<hash_map::Iter<'a, String, ByteBuf>>,
        }

        fn deserialize_key<K>(key: &str) -> DeserializerResult<K>
            where K: Deserialize,
        {
            let key_bytes = key.as_bytes();
            deserialize_slice(key_bytes)
        }
        fn deserialize_slice<V>(mut value: &[u8]) -> DeserializerResult<V>
            where V: Deserialize,
        {
            let reader = AmpReader::Limited(value.by_ref()
                .take(value.len() as u64));
            let mut deserializer = AmpDeserializer::new(reader);
            Ok(try!(Deserialize::deserialize(&mut deserializer)))
        }

        impl<'a> de::MapVisitor for MapVisitor<'a> {
            type Error = DeserializerError;

            fn visit<K, V>(&mut self) -> Result<Option<(K, V)>, Self::Error>
                where K: Deserialize,
                      V: Deserialize,
            {
                match self.iter.next() {
                    Some((key, value)) => {
                        match (deserialize_key(key),
                               deserialize_slice(value)) {
                            (Ok(key), Ok(val)) => Ok(Some((key, val))),
                            (Err(err), _) | (_, Err(err)) => Err(err.into()),
                        }
                    }
                    None => Ok(None),
                }
            }

            fn size_hint(&self) -> (usize, Option<usize>) {
                self.iter.size_hint()
            }

            // `visit_key` and `visit_value` must be implemented for this trait,
            // but we actually prefer `visit_key_value`.
            fn visit_key<K>(&mut self) -> Result<Option<K>, Self::Error>
                where K: Deserialize,
            {
                match self.iter.peek() {
                    Some(&(key, _)) => {
                        match deserialize_key(key) {
                            Ok(value) => Ok(Some(value)),
                            Err(err) => Err(err.into()),
                        }
                    }
                    None => Ok(None),
                }
            }

            fn visit_value<V>(&mut self) -> Result<V, Self::Error>
                where V: Deserialize,
            {
                match self.iter.next() {
                    Some((_, val)) => {
                        match deserialize_slice(val) {
                            Ok(value) => Ok(value),
                            Err(err) => Err(err.into()),
                        }
                    }
                    None => {
                        panic!("visit_value was called when iter was empty")
                    }
                }
            }

            fn end(&mut self) -> Result<(), Self::Error> {
                match self.iter.next() {
                    Some(_) => Err(DeserializerError::TooManyKeys),
                    None => Ok(()),
                }
            }
        }

        visitor.visit_map(MapVisitor { iter: self.0.iter().peekable() })
    }
}

/// Deserialize an object from a reader implementing `std::io::Read`,
/// returning the result
pub fn from_reader<T, R>(mut reader: R) -> Result<T, DeserializerError>
    where T: Deserialize,
          R: io::Read,
{
    let mut deserializer =
        AmpDeserializer::new(AmpReader::Unlimited(&mut reader));
    Deserialize::deserialize(&mut deserializer)
}

/// Deserialize an object from a byte slice
pub fn from_slice<T>(mut s: &[u8]) -> Result<T, DeserializerError>
    where T: Deserialize,
{
    from_reader(&mut s)
}

/// Deserialize an object from an `AmpBox`.
///
/// The keys of the `AmpBox` will be deserialized as the keys of the object,
/// except for those keys prefixed with `_`, and the values of the of the
/// `AmpBox` will be deserialized based on the type that our `Deserialize`
/// object wants.
pub fn from_box<T>(amp_box: &AmpBox) -> Result<T, DeserializerError>
    where T: Deserialize,
{
    let mut deserializer = AmpBoxDeserializer(amp_box);
    Deserialize::deserialize(&mut deserializer)
}

#[cfg(test)]
mod tests {
    use std::iter;
    use serde_crate::bytes;
    use std::collections::HashMap;
    use super::{to_vec, from_slice, Error};

    #[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
    struct Point {
        x: i32,
        y: i32,
    }

    #[test]
    fn serialize_struct() {
        let point = Point { x: 1, y: 2 };
        let serialized = to_vec(&point).unwrap();

        assert_eq!(serialized,
                   [0, 1, b'x', 0, 1, b'1', 0, 1, b'y', 0, 1, b'2', 0, 0]);
    }

    #[test]
    fn deserialize_struct() {
        let serialized = [0, 1, b'x', 0, 1, b'5', 0, 1, b'y', 0, 2, b'1',
                          b'0', 0, 0];
        let deserialized: Point = from_slice(&serialized).unwrap();
        assert!(deserialized == Point { x: 5, y: 10 });
    }

    #[test]
    fn serialize_deserialize_atomic_types() {
        #[derive(Serialize, Deserialize, Debug, PartialEq)]
        struct Diverse {
            int64: i64,
            uint64: u64,
            float64: f64,
            string: String,
            bytes: bytes::ByteBuf,
            boolean: bool,
        }

        let diverse = Diverse {
            int64: -10,
            uint64: 1000000,
            float64: -1.25E22,
            string: "This is a test of the emergency broadcast system. αβγ"
                .into(),
            bytes: b"Random gibberish \xFF\xFF\xFF".to_vec().into(),
            boolean: true,
        };
        let serialized = to_vec(&diverse).unwrap();
        let deserialized: Diverse = from_slice(&serialized).unwrap();

        assert_eq!(diverse, deserialized);
        let reserialized = to_vec(&deserialized).unwrap();
        assert_eq!(serialized, reserialized);
    }

    #[test]
    fn serialization_limits() {
        let reasonable_key = iter::repeat('a').take(255).collect::<String>();
        let long_key = iter::repeat('a').take(256).collect::<String>();
        let reasonable_val =
            iter::repeat('a').take(65535).collect::<String>();
        let long_val = iter::repeat('a').take(65536).collect::<String>();

        let mut long_key_map = HashMap::new();
        long_key_map.insert(&reasonable_key, "value");
        to_vec(&long_key_map).unwrap();

        long_key_map.insert(&long_key, "value");
        if let Error::KeyTooLong(key) = to_vec(&long_key_map).unwrap_err() {
            assert_eq!(long_key, key)
        }

        let mut long_val_map = HashMap::new();
        long_val_map.insert("a key", &reasonable_val);
        to_vec(&long_val_map).unwrap();

        long_val_map.insert("some key", &long_val);
        match to_vec(&long_val_map).unwrap_err() {
            Error::ValueTooLong(key, Some(val)) => {
                assert_eq!(key, "some key");
                assert_eq!(val, long_val);
            }
            _ => {
                panic!("expected error::ValueTooLong containing key and \
                        value")
            }
        }
    }

    #[test]
    fn serialize_list() {
        #[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
        struct Inner {
            string: String,
        }

        #[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
        struct More {
            v: Vec<Inner>,
        }

        #[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
        struct Nesty {
            flat: Vec<i32>,
            nested: Vec<Vec<bool>>,
            inner: Inner,
            vodovod: Vec<More>,
        }

        let nesty = Nesty {
            flat: vec![1, 2, 3, 4, 5],
            nested: vec![vec![true, false], vec![false, true]],
            inner: Inner { string: "ɹʌst".into() },
            vodovod: vec![More { v: vec![Inner { string: "hello".into() }] },
                          More { v: vec![] }],
        };
        let serialized = to_vec(&nesty).unwrap();
        let deserialized = from_slice(&serialized).unwrap();
        assert_eq!(nesty, deserialized)
    }

    #[test]
    fn serialize_to_box() {
        #[derive(Serialize, Deserialize, Debug, PartialEq)]
        struct Something {
            a: i32,
            b: Vec<f64>,
        }

        let amp_box = super::to_box(&Something {
                a: 5,
                b: vec![1.1, 2.2, 3.3],
            })
            .unwrap();
        assert_eq!(amp_box["a"], bytes::ByteBuf::from(b"5".to_vec()));
        assert_eq!(amp_box["b"],
                   bytes::ByteBuf::from(b"\x00\x031.1\x00\x032.2\x00\x033.3".to_vec()));
    }

    #[test]
    fn deserialize_from_box() {
        #[derive(Serialize, Deserialize, Debug, PartialEq)]
        struct Another {
            seq: Vec<String>,
            map: HashMap<String, String>,
            seqmap: Vec<HashMap<String, String>>,
        }

        let mut amp_box = HashMap::new();
        amp_box.insert(
            "seq".into(),
            bytes::ByteBuf::from(b"\x00\x03foo\x00\x03bar".to_vec())
        );
        amp_box.insert(
            "map".into(),
            bytes::ByteBuf::from(b"\x00\x03baz\x00\x04quux\x00\x00".to_vec())
        );
        amp_box.insert(
            "seqmap".into(),
            bytes::ByteBuf::from(b"\x00\x05lorem\x00\x05ipsum\x00\x00\
                                   \x00\x05dolor\x00\x03sit\x00\x00".to_vec())
        );

        let a: Another = super::from_box(&amp_box).unwrap();

        assert_eq!(a.seq, ["foo", "bar"]);
        assert_eq!(a.map["baz"], "quux");
        assert_eq!(a.seqmap[0]["lorem"], "ipsum");
        assert_eq!(a.seqmap[1]["dolor"], "sit");
    }

    #[test]
    fn box_roundtrip() {
        #[derive(Serialize, Deserialize, Debug, PartialEq)]
        struct TestBox {
            _ask: String,
            _command: String,
            arg1: String,
            arg2: String,
        }

        let orig = TestBox {
            _ask: "9AD9A3A1F37690B806D94CC2DFE8BAB8D47F21F1E37DC7522E2A1FA4EC3F1321".into(),
            _command: "Test".into(),
            arg1: "something".into(),
            arg2: "another".into(),
        };

        let amp_box = super::to_box(&orig).unwrap();
        let serialized = super::to_vec(&amp_box).unwrap();
        let deserialized: super::AmpBox = super::from_slice(&serialized)
            .unwrap();
        let last: TestBox = super::from_box(&deserialized).unwrap();

        assert_eq!(orig, last);
    }
}
