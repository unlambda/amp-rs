//! Traits and macro for defining a `Command`, the interface between client
//! and server.
//!
//! In AMP, every message is a command, with a response.  The interface for
//! the command is defined once, and the client and server use that interface
//! to serialize and deserialize the message appropriately.
//!
//! In Rust, the interface is defined by implementing the trait `Command`.  It
//! can be implemented by a zero-sized struct, as it is merely serves to carry
//! associated types for defining the argument, response, and error types, and
//! a method to get the command name.
//!
//! Because defining the argument type, response type, error type, and adding
//! all of the appropriate derivations, and mappings from error codes to the
//! appropriate variant would be cumbersome, the [`command!`] macro is
//! provided to simplify the process.
//!
//! For example, the following might be how you represent a command for
//! sending a message to a list of recipients:
//!
//! ```
//! #![feature(custom_derive, plugin)]
//! #![plugin(serde_macros)]
//!
//! #[macro_use]
//! extern crate amp;
//!
//! # fn main() {
//! command! {
//!     // Define a zero-sized struct SendMessage, and implement `Command`
//!     // for it.  The `name` method will return "SendMessage".
//!     struct SendMessage {
//!
//!         // Define the associated argument type.  Defines a struct with
//!         // the given fields.
//!         arguments: SendArgs {
//!             title: String,
//!             recipients: Vec<String>,
//!             body: String
//!         },
//!
//!         // Response type, defined the same way as the argument type.
//!         response: SendResponse {
//!             success: Vec<bool>
//!         },
//!
//!         // An enum listing the different errors.  Each name will
//!         // correspond to an enum variant, as well as the code used to
//!         // represent it.  Each enum variant will also carry a String
//!         // for a description.  The last variant, which must be
//!         // distinguished with "..", will be used for any errors that
//!         // are unknown.
//!         errors: SendError {
//!             INVALID_RECIPIENT,
//!             SERVER_DOWN,
//!             MESSAGE_TOO_LARGE,
//!             ..UNKNOWN
//!         }
//!     }
//! }
//! # ()
//! # }
//! ```
//!
//! [`command!`]: ../macro.command!.html

use std::error;
use serde_crate::{Serialize, Deserialize};

/// The string used to identify any uknown errors that might occur, that we
/// don't have a mapping for, when serializing an error.
pub const UNKNOWN_ERROR: &'static str = "UNKNOWN";

/// A Command represent the interface for a single command, including
/// associated types for the argument type, response type, and error type, as
/// well as a method that returns the command name as a string.
pub trait Command {
    /// A type representing the command argument.  Must serialize as a map, so
    /// a struct or map with string keys is required.
    type Argument: Serialize + Deserialize;
    /// A type representing the command response.  Must also serialize as a map.
    type Response: Serialize + Deserialize;
    /// The error type.  Must implement our `Error` trait.  Usually an `enum`
    /// in which the code maps to different variants, and the description is
    /// preserved.
    type Error: Error;

    fn name(&self) -> &str;
}

/// Our `Error` trait, which also requires an implementation of `std::error::Error`
///
/// Used to translate from the representation of an error that AMP provides
/// and a Rust `Error` that can be used in a `Result` with all of the benefits
/// that `try!` gives you as well.
pub trait Error: error::Error {
    fn from_code_description(code: &str, description: &str) -> Self;
    fn to_code_description(&self) -> (&str, &str);
}

/// Helper macro to reduce the boilerplate needed to define a command and
/// implement all of the associated types and their traits.
///
/// See the [`amp::command`] module documentation for details.
///
/// [`amp::command`]: command/index.html
#[macro_export]
macro_rules! command {
    // Rust complains about "local ambiguity: multiple parsing options:
    // built-in NTs ident ('err') or 1 other option" if we try to use the
    // error pattern we want to use, { $( $err:ident , )* ..$err_unknown:ident
    // }, so use some helper expansions to manually parse off one ident at a
    // time until reaching a base case, and at that point provide a form
    // that the macro parser can match easily with extra parentheses.
    ( struct $name:ident {
        arguments: $arg_name:ident { $($args:tt)* },
        response: $res_name:ident { $($response:tt)* },
        errors: $err_name:ident { $($err_defs:tt)* }
    }) => {
        command! {
            @parse_errors [$name $arg_name $res_name $err_name]
                [$($args)*] [$($response)*]
                () { $($err_defs)* }
            }
        };
    ( @parse_errors [$name:ident $arg_name:ident $res_name:ident $err_name:ident]
       [$($args:tt)*] [$($response:tt)*]
       ($($known_errs:tt)*) { ..$err_unknown:ident })
        => {
            command! {
                @final_expansion [$name $arg_name $res_name $err_name]
                    [$($args)*] [$($response)*]
                    ($($known_errs)*) ..$err_unknown
            }
        };
    ( @parse_errors [$name:ident $arg_name:ident $res_name:ident $err_name:ident]
       [$($args:tt)*] [$($response:tt)*]
       ($($known_errs:tt)*) { $err:ident , $($more_errs:tt)* })
        => {
            command! {
                @parse_errors [$name $arg_name $res_name $err_name]
                    [$($args)*] [$($response)*]
                    ($($known_errs)* $err ,) { $($more_errs)* }
            }
        };
    ( @final_expansion
       [$name:ident $arg_name:ident $res_name:ident $err_name:ident]
       [$($arg:ident : $arg_ty:ty),*]
       [$($res_arg:ident : $res_arg_ty:ty),*]
       ($( $err:ident ,)*)
       ..$err_unknown:ident )
        => {
            struct $name;
            #[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
            struct $arg_name {
                $($arg : $arg_ty),*
            }
            #[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
            struct $res_name {
                $($res_arg : $res_arg_ty),*
            }
            #[allow(non_camel_case_types)]
            #[derive(Debug, Eq, PartialEq)]
            enum $err_name {
                $($err(::std::string::String),)*
                $err_unknown(::std::string::String),
            }
            impl ::std::fmt::Display for $err_name {
                fn fmt(&self, f: &mut ::std::fmt::Formatter)
                       -> ::std::result::Result<(), ::std::fmt::Error>
                {
                    use $crate::command::Error;
                    let (code, description) = self.to_code_description();
                    write!(f, "AMP error in {}: {}: {}",
                           stringify!($name), code, description)
                }
            }
            impl ::std::error::Error for $err_name {
                fn description(&self) -> &str {
                    match self {
                        $(&$err_name::$err(ref description) => &description,)*
                        &$err_name::$err_unknown(ref description) => &description,
                    }
                }
            }
            impl $crate::command::Error for $err_name {
                fn from_code_description(code: &str, description: &str) -> Self {
                    match code {
                        $(stringify!($err) => $err_name::$err(description.into()),)*
                        _ => $err_name::$err_unknown(description.into()),
                    }
                }
                fn to_code_description(&self) -> (&str, &str) {
                    match self {
                        $(&$err_name::$err(ref description)
                          => (stringify!($err), &description),)*
                        &$err_name::$err_unknown(ref description)
                          => ($crate::command::UNKNOWN_ERROR, &description),
                    }
                }
            }
            impl $crate::command::Command for $name {
                type Argument = $arg_name;
                type Response = $res_name;
                type Error = $err_name;
                fn name(&self) -> &'static str { stringify!($name) }
            }

        }
}

#[cfg(test)]
mod tests {
    use super::{Command, Error};

    #[test]
    fn define_command() {
        command! {
            struct Add {
                arguments: AddArgs { a: i32, b: i32 },
                response: AddRes { result: i32 },
                errors: AddError { FOO, BAR, ..UNKNOWN }
            }
        }
        command! {
            struct Div {
                arguments: DivArgs { x: i32, y: i32 },
                response: DivRes { div: i32, rem: i32 },
                errors: DivError { DIV_BY_ZERO, ..UNKNOWN }
            }
        }

        assert_eq!(Add.name(), "Add");
        assert_eq!(Div.name(), "Div");

        assert_eq!(AddError::FOO("blah".into()).to_code_description(),
                   ("FOO", "blah"));
        assert_eq!(
            AddError::from_code_description(
                "BAR",
                "A priest, a rabbi, and a..."
            ),
            AddError::BAR("A priest, a rabbi, and a...".into())
        );
        assert_eq!(
            AddError::from_code_description("OTHER", "He's dead, Jim!"),
            AddError::UNKNOWN("He's dead, Jim!".into())
        );
    }

    #[test]
    #[allow(dead_code)]  // Suppress warning that Ping is unused
    fn serialize_command() {
        use serde::{to_vec, from_slice};

        command! {
            struct Ping {
                arguments: PingArgs { hops: u8 },
                response: PingRes { latency: u64 },
                errors: PingError { ..UNKNOWN }
            }
        }

        let args = PingArgs { hops: 10 };
        let serialized = to_vec(&args).unwrap();
        let deserialized = from_slice(&serialized).unwrap();
        assert_eq!(args, deserialized);

        let res = PingRes { latency: 1200 };
        let serialized = to_vec(&res).unwrap();
        let deserialized = from_slice(&serialized).unwrap();
        assert_eq!(res, deserialized);
    }
}
