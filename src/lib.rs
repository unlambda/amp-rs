//! [AMP] is a protocol for asynchronous RPC originally implemented in
//! [Twisted], a framework for asynchronous networking in Python.  See the
//! [Twisted AMP HOWTO] and [Twisted API docs] for details on how it is
//! intended to be used.  The protocol is described in detail at
//! http://amp-protocol.net.
//!
//! This crate implements serialization and deserialization of AMP via
//! [`serde`] in the [`serde` module].  This allows simply decorating
//! structures with `#[derive(Serialize, Deserialize)]` to allow sending
//! them in AMP messages.
//!
//! The AMP protocol is structured around commands, which define an interface
//! that can be used by sender and receiver to define the accepted argument,
//! response, and error types.  This interface is defined in the [`command`]
//! module.
//!
//! To actually communicate using AMP, you must set up an entpoint.  The
//! endpoint can have handlers registered, specifying a command and a closure
//! to handle the given command, for commands initiated from the other side of
//! the connection; or you can call an arbitrary command on the enpoint.
//! Support for setting up endpoints is defined in [`protocol`].
//!
//! [AMP]: https://amp-protocol.net
//! [Twisted]: http://twistedmatrix.com/
//! [Twisted AMP HOWTO]: http://twistedmatrix.com/documents/current/core/howto/amp.html
//! [Twisted API docs]:
//! http://twistedmatrix.com/documents/current/api/twisted.protocols.amp.html
//! [`serde`]: https://github.com/serde-rs/serde
//! [`serde` module]: serde/index.html
//! [`command`]: command/index.html
//! [`protocol`]: protocol/index.html

#![cfg_attr(test, feature(custom_derive, plugin))]
#![cfg_attr(test, plugin(serde_macros))]

extern crate serde as serde_crate;
extern crate byteorder;
extern crate rand;
extern crate data_encoding;

pub mod serde;
#[macro_use]
pub mod command;
pub mod protocol;
pub mod util;

pub use protocol::SyncEndpoint;
