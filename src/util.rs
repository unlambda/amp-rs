//! Handy utilities for protocol debugging

use std::collections::VecDeque;
use std::io;
use std::io::prelude::*;
use std::sync::mpsc::{channel, Sender, Receiver};

/// Print out a hex dump of an `&[u8]` to stderr, containing both the hex
/// values and ASCII value if printable.
///
/// Modified from the [version posted on Reddit][reddit] by [/u/birkenfeld][birk]
///
/// [reddit]: https://www.reddit.com/r/rust/comments/4ehqo3/simple_hexdump_function_to_help_those_working/d209yki
/// [birk]: https://www.reddit.com/user/birkenfeld
pub fn hexdump(data: &[u8]) {
    for chunk in data.chunks(16) {
        let mut numeric = String::with_capacity(50);
        let mut ascii = String::with_capacity(16);
        for (i, &b) in chunk.iter().enumerate() {
            numeric.push_str(&format!("{:02X} ", b));
            if i == 7 {
                numeric.push(' ');
            }
            ascii.push(match b {
                0x20...0x7e => b as char,
                _ => '.',
            });
        }
        let _ = writeln!(io::stderr(), "{:50}{}", numeric, ascii);
    }
}

/// Wrap a reader, dumping out the value of every read separated by blank
/// lines (to distinguish separate reads from longer reads)
pub struct DumpRead<R>(pub R);

/// Wrap a writer, dumping out the value of every write separated by blank
/// lines
pub struct DumpWrite<W>(pub W);


/// Wrap stream, that implements Read + Write, dumping out the value of each
/// read or write, prefixed by which it is to distinguish the two directions
pub struct DumpStream<S>(pub S);

impl<R: Read> Read for DumpRead<R> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let read = try!(self.0.read(buf));
        hexdump(buf.split_at(read).0);
        let _ = writeln!(io::stderr(), "");
        Ok(read)
    }
}

impl<W: Write> Write for DumpWrite<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let written = try!(self.0.write(buf));
        hexdump(buf.split_at(written).0);
        let _ = writeln!(io::stderr(), "");
        Ok(written)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.0.flush()
    }
}

impl<S: Read> Read for DumpStream<S> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let _ = writeln!(io::stderr(), "read {}", buf.len());
        DumpRead(&mut self.0).read(buf)
    }
}

impl<S: Write> Write for DumpStream<S> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let _ = writeln!(io::stderr(), "write {}", buf.len());
        DumpWrite(&mut self.0).write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.0.flush()
    }
}

/// The reading end of a pipe returned by `pipe`
pub struct PipeReader(Receiver<Vec<u8>>, VecDeque<u8>);
/// The writing end of a pipe returned by `pipe`
#[derive(Clone)]
pub struct PipeWriter(Sender<Vec<u8>>);

/// Return a pair of a reader and writer, which implement `Read` and `Write`
/// respectively, and which can be used to communicate between two threads in
/// the same process.  Based loosely on the [pipe crate][pipe], but adds
/// unlimited buffering to avoid certain deadlocks, doesn't use `unsafe`, and
/// uses the standard library's `VecDeque` to simplify the buffering logic and
/// avoid dropping bytes like the pipe crate does.
///
/// [pipe]: https://github.com/arcnmx/pipe-rs
pub fn pipe() -> (PipeReader, PipeWriter) {
    let (tx, rx) = channel();

    (PipeReader(rx, VecDeque::new()), PipeWriter(tx))
}

impl Read for PipeReader {
    fn read(&mut self, mut buf: &mut [u8]) -> io::Result<usize> {
        let mut written = 0;

        while buf.len() > 0 {
            // First, write from previously buffered data into the out buf
            let mut consumed = 0;
            {
                let (first, second) = self.1.as_slices();
                // Safe to unwrap since writing to a slice can't fail
                consumed += buf.write(first).unwrap();
                consumed += buf.write(second).unwrap();
            }
            self.1.drain(..consumed);
            written += consumed;
            if buf.len() == 0 {
                break;
            }

            if let Ok(data) = self.0.recv() {
                let used = buf.write(&data[..]).unwrap();
                written += used;
                self.1.extend(&data[used..]);
            }
        }

        Ok(written)
    }
}

impl Write for PipeWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let v = buf.to_vec();
        match self.0.send(v) {
            Ok(_) => Ok(buf.len()),
            Err(_) => Ok(0),
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use std::io::prelude::*;
    use super::pipe;

    #[test]
    fn test_pipe() {
        let (mut read, mut write) = pipe();
        let result = [1u8; 64];

        let guard = ::std::thread::spawn(move || {
            let mut buf = [0u8; 64];
            let mut nread = read.read(&mut buf[..16]).unwrap();
            nread += read.read(&mut buf[16..32]).unwrap();
            nread += read.read(&mut buf[32..]).unwrap();
            assert_eq!(nread, 64);
            assert_eq!(&buf[..], &result[..]);
        });

        let mut nwritten = write.write(&result[..32]).unwrap();
        nwritten += write.write(&result[32..48]).unwrap();
        nwritten += write.write(&result[48..]).unwrap();
        assert_eq!(nwritten, 64);
        guard.join().unwrap();
    }

}
